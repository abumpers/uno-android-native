package com.example.googlemapszoomradius;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSeekBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.w3c.dom.Text;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private SeekBar seekBar;
    private TextView textView;
    float thirtyMiles = 48280;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        initializeVariables();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // TODO: Change to user location
        // Add a marker in Pike Place and move the camera.
        LatLng pikePlace = new LatLng(47.6062, -122.3321);
        final CircleOptions circleOptions = new CircleOptions(); //remove final

        final Circle circle;
        circle = mMap.addCircle(new CircleOptions()
                .center(pikePlace)
                .fillColor(Color.RED)
                //.radius(100)
                .strokeWidth(3)
                .strokeColor(0xFF9BFAF1)
                .fillColor(Color.parseColor("#509BFAF1")));


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            int progress = 0;
            // ~1609 meters = 1 mile
            // sets radius to a max of 30.0 miles from center point
            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                progress = progressValue;
                circle.setRadius(progress * 1609);
                String circleString = String.valueOf(circle.getRadius() / 1609);
                Toast.makeText(getApplicationContext(), circleString + "miles", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                circle.setRadius(progress * 1609);
                String circleString = String.valueOf(circle.getRadius() / 1609);
                Toast.makeText(getApplicationContext(), circleString + " miles", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                circle.setRadius(progress * 1609);
                String circleString = String.valueOf(circle.getRadius() / 1609);
                Toast.makeText(getApplicationContext(), circleString + "miles", Toast.LENGTH_SHORT).show();
            }
        });

        mMap.addMarker(new MarkerOptions()
                .position(pikePlace)
                .title("Pike Place") // placeholder
                .snippet("Alex") // placeholder
                .anchor(0.5f, 1)
        );

        float zoomLevel = 12.0f;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pikePlace, zoomLevel));


    }

    private void initializeVariables() {
        seekBar = (SeekBar) findViewById(R.id.seekBar1);
        textView = (TextView) findViewById(R.id.textView1);
    }
};

